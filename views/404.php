<?php $this->layout('template.base', ['title' => $title]) ?>

<h1>404</h1>
<div class="content">
    <p>The requested page could not be found :(</p>
</div>
