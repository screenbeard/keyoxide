<?php $this->layout('template.base', ['title' => $title]) ?>

<h1>Guides</h1>
<div class="content">
    <h3>Using Keyoxide</h3>
    <a href="/guides/verify">Verifying a signature</a><br>
    <a href="/guides/encrypt">Encrypting a message</a><br>
    <a href="/guides/proofs">Verifying identity proofs</a><br>
    <a href="/guides/contributing">Contributing to Keyoxide</a><br>
    <a href="/guides/migrating-from-keybase">Migrating from Keybase</a><br>
    <a href="/guides/feature-comparison-keybase">Feature comparison with Keybase</a><br>

    <h3>Beyond Keyoxide</h3>
    <a href="/guides/openpgp-proofs">How OpenPGP identity proofs work</a><br>
    <a href="/guides/web-key-directory">Uploading keys using web key directory</a><br>
    <a href="/guides/self-hosting-keyoxide">Self-hosting Keyoxide</a><br>
    <a href="/guides/service-provider">Are you a service provider?</a><br>

    <h3>Adding proofs</h3>
    <a href="/guides/dns">Adding a DNS proof</a><br>
    <a href="/guides/mastodon">Adding a Mastodon proof</a><br>
    <a href="/guides/twitter">Adding a Twitter proof</a><br>
    <a href="/guides/lobsters">Adding a Lobste.rs proof</a><br>
    <a href="/guides/hackernews">Adding a Hackernews proof</a><br>
    <a href="/guides/reddit">Adding a Reddit proof</a><br>
    <a href="/guides/github">Adding a Github proof</a><br>
    <a href="/guides/xmpp">Adding a XMPP proof</a><br>
</div>
