<p>Though it's not a fully supported use case yet, anyone can take the <a href="https://codeberg.org/yarmo/keyoxide">source code</a> and put it on their own server. The idea is that <a href="https://keyoxide.org">Keyoxide.org</a> is not special in itself. After all, all the heavy lifting is done by the browser. So the role of any individual Keyoxide server is to get the tool in the hands of the end user.</p>

<p>The few supporting roles the server has can easily be performed by any other (PHP) server.</p>

<p>So if you like the project but perhaps are mistrusting of servers of others, especially when it comes to keypairs, here's the <a href="https://codeberg.org/yarmo/keyoxide">source code</a> and put it on your own server. Thanks for using the project!</p>
