<p>Let's see how easy it is to get a Keyoxide profile when you already have a Keybase account.</p>

<h3>Claim your Keyoxide profile</h3>

<p>Have you made your public key accessible through <strong>web key directory</strong> (WKD) on your website or uploaded it to <a href="https://keys.openpgp.org/">keys.openpgp.org</a>? Congratulations, you now have your very own Keyoxide profile!</p>

<h3>Link to your Keyoxide profile</h3>

<p>
    Append your OpenPGP key's fingerprint to the domain as such:<br>
    <strong><?=$this->e($base)?>/FINGERPRINT</strong><br>and there's your profile page with the public key fetched from <a href="https://keys.openpgp.org/">keys.openpgp.org</a>.
</p>

<p>
    If you wish to use WKD instead, simply append your email address as such:<br>
    <strong><?=$this->e($base)?>/USER@DOMAIN.ORG</strong>
</p>

<p>
    Lastly, if you still wish your key to be fetched from <a href="https://keys.openpgp.org/">keys.openpgp.org</a> but using your email address instead of the key's fingerprint, simply override by using the following URL:<br>
    <strong><?=$this->e($base)?>/hkp/USER@DOMAIN.ORG</strong>
</p>
