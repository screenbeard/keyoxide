# Keyoxide

[Keyoxide](https://keyoxide.org) is a FOSS solution to make basic cryptography operations accessible to regular humans.

[![Build Status](https://drone.private.foss.best/api/badges/yarmo/keyoxide/status.svg?branch=main)](https://drone.private.foss.best/yarmo/keyoxide)
